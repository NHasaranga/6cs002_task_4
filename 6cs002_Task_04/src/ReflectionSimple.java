public class ReflectionSimple 
{
	public double in1 = 50.5;
	private double in2 = 25.4;
	
	public ReflectionSimple() 
	{
		
	}

public ReflectionSimple(double in1, double in2) 
{
	this.in1 = in1;
	this.in2 = in2;
}

	public void sqrtin1() 
	{
		this.in1 = Math.sqrt(this.in1);
	}

private void sqrtin2() 
{
	this.in2 = Math.sqrt(this.in2);
}

	public double getin1() 
	{
		return in1;
	}
	
private void setin1(double in1) 
{
this.in1 *= this.in1;
}
	public double getin2() 
	{
		return in2;

	}
	
public void setin2(double in2) 
{
this.in2 *= this.in2;
}

	public String toString() 
	{
		return String.format("in1 : %.2f\nin2 : %.2f", in1, in2);
	}
}