import java.lang.reflect.Field;
public class Reflection07 
{
	public static void main(String[] args) throws Exception 
	{
			ReflectionSimple objct = new ReflectionSimple();
			System.out.println("... Reflection 07 ...");
			Field [] fields = objct.getClass().getDeclaredFields();
			System.out.printf("There are %d fields\n", fields.length);
				for(Field f : fields) 
				{
					f.setAccessible(true);
					System.out.printf("field name = %s, type = %s, value = %.2f\n", f.getName(), 
							f.getType(), f.getDouble(objct));
				}
	}
}
