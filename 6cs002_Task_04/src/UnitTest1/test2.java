package UnitTest1;

 
import static UnitTest2.test.*;

 
public class test2 {

 

  void checkvaluedouble(){
      test1 s = new test1(46.9, 12.4);
      checkEquals(s.getfst_val(), 46.9);
      checkEquals(s.getsec_val(), 12.4);
      checkNotEquals(s.getsec_val(), 12.4);    
      checkNotEquals(s.getsec_val(), 46.9);    
  }
  
  
  void checkvaluestring(){
		test1 test1 = new test1(46.9, 12.4);
		  checkStringCharAt("IT2049389", "IT0989");
		  test1 = new test1(45.7, 0);
		  checkStringCharAt("Naveen", "Hasaranaga");
		  test1 = new test1(45.7, 0);
		  checkStringCharAt("N", "H");
		  
	}
 

    void checkSquareA(){
        test1 object = new test1(25.5, 45.5);
        object.squarefst_val();
        checkEquals(object.getfst_val(), 25.3);
    }

 

  public static void main(String[] args) {
    test2 Objecttest = new test2();
    Objecttest.checkvaluedouble();
    Objecttest.checkSquareA();
    Objecttest.checkvaluestring();
    report();
  }
}