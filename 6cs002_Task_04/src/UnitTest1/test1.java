package UnitTest1;

public class test1 
{
    public double fst_val = 46.9;
    private double sec_val = 12.4;
    public test1() 
    {
    	
    }
    public test1(double fst_val, double sec_val) 
    	{
        	this.fst_val = fst_val;
        	this.sec_val = sec_val;
    	}
    public void squarefst_val() 
    	{
        	this.fst_val = Math.sqrt(this.fst_val);
    	}    
    private void squaresec_val() 
    	{
        	this.sec_val = Math.sqrt(this.sec_val);
    	}    
    public double getfst_val() 
    	{
        	return fst_val;
    	}    
    private void setfst_val(double fst_val) 
    	{
        
    		this.fst_val = fst_val;
    	}    
    public double getsec_val() 
    	{
        
    		return sec_val;
    	}    
    public void setsec_val(double b) 
    	{
        	this.sec_val = b;
    	}    
    public String toString() 
    	{
        	return String.format("\n1st Value : %.2f\n2nd Value : %.2f", fst_val, sec_val);
    	}
}