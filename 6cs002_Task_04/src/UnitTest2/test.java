package UnitTest2;

import java.util.*;

public class test 
{
  private static List<String> checks;
  private static int Checks = 0;
  private static int Passed = 0;
  private static int Failed = 0;

 

  private static void Statement(String txt) 
  {
    
	  if (checks == null) 
	  {
		  checks = new LinkedList<String>();
	  }
    
	  checks.add(String.format("%04d: %s", Checks++, txt));
  }
  public static void checkStringCharAt1(String value1, String value2) {
	  try {
	        char n1 = value1.charAt(5);
	        char n2 = value2.charAt(5);
	        if (value1.length() >= 5 && value2.length() >= 5) {
	            if (Character.isDigit(n1) || Character.isDigit(n2)) {
	            	Statement(String.format("%s and %s contain letters and numbers", value1, value2));
	                Checks++;
	            } else if (value1.charAt(5) == n1 && value2.charAt(5) == n2) {
	            	Statement(String.format("%s and %s contain the expected characters", value1, value2));
	                Passed++;
	            } else {
	            	Statement(String.format("%s and %s do not contain the expected characters", value1, value2));
	                Failed++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {
	        System.out.println("Both strings must be more than 5 letters");
	    }
	}
 

  public static void checkEquals(double value1, double value2) 
  {
        if (value1 == value2) 
        {
            Statement(String.format("  %.2f == %.2f", value1, value2));
            Passed++;
        } else {
            Statement(String.format("* %.2f == %.2f", value1, value2));
            Failed++;
        	}
   }

 

  public static void checkNotEquals(double value1, double value2) 
  {
        if (value1 != value2) 
        {
            Statement(String.format("  %.2f != %.2f", value1, value2));
            Passed++;
        } else {
            Statement(String.format("* %.2f != %.2f", value1, value2));
            Failed++;
        		}
   }
  
  public static void checkStringCharAt(String value1, String value2) 
  {
        try {
            char n1 = value1.charAt(4);
            char n2 = value2.charAt(4);
            if (value1.charAt(4) == n1 && value2.charAt(4) == n2) 
            {
                Statement(String.format("%s and %s more than 4 letters", value1, value2));
                Passed++;
            } else {
                Statement(String.format("%s and %s more than 4 letters", value1, value2));
                Failed++;
            		}
        	}catch(StringIndexOutOfBoundsException e) {
            
            Statement(String.format("Inserted String must be more than 4 letters\n"));
            Failed++;
        }
        
      }

 
  public static void report() 
  {
      
    System.out.printf("%d .. Passed Checks\n", Passed);
    System.out.printf("%d .. Failed Checks\n", Failed);
    System.out.println();
    
    for (String check : checks) 
    {
      System.out.println(check);
    }
  }
}
 