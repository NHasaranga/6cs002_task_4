import java.lang.reflect.Method;

public class Reflection10 {
    public static void main(String[] args) throws Exception 
    {
        
        ReflectionSimple objct = new ReflectionSimple();
        
        System.out.println("... Reflection 10 ...");
        Method method = objct.getClass().getDeclaredMethod("setIn 2 : ", double.class);
 
        method.setAccessible(true);
        method.invoke(objct, (double) 24.1);
        System.out.println(objct);
        
      }
}