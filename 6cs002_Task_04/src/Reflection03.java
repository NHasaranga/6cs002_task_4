public class Reflection03 
{
	public static void main(String[] args) 
	{
		ReflectionSimple objct = new ReflectionSimple();
		System.out.println("... Reflection 03 ...");
		System.out.println("class = " + objct.getClass());
		System.out.println("class name = " + objct.getClass().getName());
	}
}
